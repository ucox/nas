module ucox.cn/nas

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/StackExchange/wmi v0.0.0-20181212234831-e0a55b97c705 // indirect
	github.com/gin-contrib/zap v0.0.0-20190528085758-3cc18cd8fce3
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/core v0.6.2
	github.com/go-xorm/xorm v0.7.1
	github.com/mattn/go-isatty v0.0.7
	github.com/mitchellh/mapstructure v1.1.2
	github.com/shirou/gopsutil v2.18.12+incompatible
	github.com/spf13/viper v1.4.0
	go.uber.org/zap v1.10.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
