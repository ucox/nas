package route

import (
	"github.com/gin-gonic/gin"
	"ucox.cn/nas/config"
	"ucox.cn/nas/modules/console"
)

func RegisterConsoleApi(engine *gin.Engine) {

	//root module
	rootRouteGroup := engine.Group(config.App.ServerConfig.WebRoot)
	{
		//console module
		consoleRouteGroup := rootRouteGroup.Group("console")
		{
			consoleRouteGroup.GET("index", console.Index)
		}
	}
}
