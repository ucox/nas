package route

import (
	"github.com/gin-gonic/gin"
	. "ucox.cn/nas/config"
	"ucox.cn/nas/modules/system"
)

// Use route of system module
func RegisterSystemApi(engine *gin.Engine) {
	rootRouteGroup := engine.Group(App.ServerConfig.WebRoot)
	{
		systemRouteGroup := rootRouteGroup.Group("system")
		{
			systemRouteGroup.GET("dir", system.ListDirs)
			systemRouteGroup.GET("mem", system.MemoryInfo)
			systemRouteGroup.GET("disk", system.DiskInfo)
			systemRouteGroup.GET("disk/io", system.DiskIO)
			systemRouteGroup.GET("cpu", system.CpuInfo)
		}
	}
}
