package route

import (
	"github.com/gin-gonic/gin"
	"ucox.cn/nas/config"
	"ucox.cn/nas/modules/carte"
)

func RegisterCarteApi(engine *gin.Engine) {

	//root module
	rootRouteGroup := engine.Group(config.App.ServerConfig.WebRoot)
	{
		carteRouteGroup := rootRouteGroup.Group("carte")
		{
			carteRouteGroup.GET("categories", carte.Categories)
			carteRouteGroup.GET("new", carte.SaveCategory)
		}
	}
}
