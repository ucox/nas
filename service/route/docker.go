package route

import (
	"github.com/gin-gonic/gin"
	"ucox.cn/nas/config"
	"ucox.cn/nas/modules/docker"
)

func RegisterDockerApi(engine *gin.Engine) {
	rootRouteGroup := engine.Group(config.App.ServerConfig.WebRoot)
	{
		//docker management
		dockerRouteGroup := rootRouteGroup.Group("docker")
		{
			dockerRouteGroup.GET("containers", docker.GetContainers)
			dockerRouteGroup.GET("containers/:id", docker.GetContainer)
			dockerRouteGroup.GET("containers/:id/top", docker.GetContainerTop)
			dockerRouteGroup.POST("containers/:id/start", docker.StartContainer)
			dockerRouteGroup.POST("containers/:id/stop", docker.StopContainer)
			dockerRouteGroup.DELETE("containers/:id", docker.DeleteContainer)
			dockerRouteGroup.GET("containers/:id/stats", docker.GetContainerStats)
			dockerRouteGroup.GET("images", docker.GetImages)
			//docker.GET("images", dockerApi.GetUserImage)
			dockerRouteGroup.GET("images/:id", docker.GetImage)
			dockerRouteGroup.GET("search", docker.SearchImages)
			dockerRouteGroup.DELETE("images/:id", docker.DeleteImage)
			dockerRouteGroup.GET("version", docker.GetVersion)
			dockerRouteGroup.GET("info", docker.GetDockerInfo)
			dockerRouteGroup.GET("ping", docker.GetDaemonPing)
		}
	}
}
