package utils

import (
	"fmt"
	"reflect"
)

func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}

func setField(target interface{}, key string, value interface{}) error {
	structData := reflect.ValueOf(target).Elem()
	fieldValue := structData.FieldByName(key)

	if !fieldValue.IsValid() {
		return fmt.Errorf("utils.setField() No such field: %s in target ", key)
	}

	if !fieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value ", key)
	}

	fieldType := fieldValue.Type()
	val := reflect.ValueOf(value)

	valTypeStr := val.Type().String()
	fieldTypeStr := fieldType.String()
	if valTypeStr == "float64" && fieldTypeStr == "int" {
		val = val.Convert(fieldType)
	} else if fieldType != val.Type() {
		return fmt.Errorf("Provided value type " + valTypeStr + " didn't match target field type " + fieldTypeStr)
	}
	fieldValue.Set(val)
	return nil
}

// SetStructByJSON 由json对象生成 struct
func SetStructByJSON(target interface{}, source map[string]interface{}) error {
	for key, value := range source {
		if err := setField(target, key, value); err != nil {
			fmt.Println(err.Error())
			return err
		}
	}
	return nil
}
