package utils

import (
	"fmt"
	"testing"
)

func TestFib(t *testing.T) {
	var (
		in       = 7
		expected = 13
	)
	actual := Fib(in)
	if actual != expected {
		t.Errorf("Fib(%d)=%d;expected %d", in, actual, expected)
	}
}

func ExampleFib() {
	fmt.Println(Fib(7))
	// Output: 13
}

func BenchmarkFib10(b *testing.B) {
	//b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Fib(10)
	}
}
