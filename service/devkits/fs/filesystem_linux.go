//+build linux

package fs

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"strconv"
	"strings"
	"syscall"
	"time"
)

//const (
//	DataTimeFormatLayout = "2006-01-02 15:04:05"
//)
//
//type FileItem struct {
//	Name       string `json:"name"`
//	Type       string `json:"type"`
//	Owner      string `json:"owner"`
//	IsHidden   bool   `json:"hidden"`
//	Permission string `json:"permission"`
//	Group      string `json:"group"`
//	Size       int64  `json:"size"`
//	ModifyTime string `json:"mtime"`
//	CreateTime string `json:"ctime"`
//	AccessTime string `json:"atime"`
//}

func ListDirectories(path string) ([]DirInfo, error) {
	rd, err := ioutil.ReadDir(path)
	var rt []DirInfo
	if err != nil {
		return rt, err
	}
	for _, fi := range rd {
		fmt.Printf("Name=%s Permission=%s, Type:%d,\n", fi.Name(), fi.Mode()&os.ModePerm, fi.Mode()&os.ModeSymlink)
		mode := fi.Mode()
		dirInfo := DirInfo{
			Name:       fi.Name(),
			Size:       fi.Size(),
			Permission: strings.TrimPrefix(fmt.Sprintf("%s", mode&os.ModePerm), "-"),
			ModifyTime: fi.ModTime().Format(DataTimeFormatLayout),
		}

		sysInfo := fi.Sys().(*syscall.Stat_t)
		if u, err := user.LookupId(strconv.Itoa(int(sysInfo.Uid))); nil == err {
			dirInfo.Owner = u.Username
		}
		if u, err := user.LookupGroupId(strconv.Itoa(int(sysInfo.Gid))); nil == err {
			dirInfo.Group = u.Name
		}
		dirInfo.CreateTime = time.Unix(sysInfo.Ctim.Sec, sysInfo.Ctim.Nsec).Format(DataTimeFormatLayout)
		dirInfo.AccessTime = time.Unix(sysInfo.Ctim.Sec, sysInfo.Ctim.Nsec).Format(DataTimeFormatLayout)
		dirInfo.ModifyTime = time.Unix(sysInfo.Ctim.Sec, sysInfo.Ctim.Nsec).Format(DataTimeFormatLayout)
		rt = append(rt, dirInfo)
	}
	return rt, err
}

func ListDirectoryItem(path string, showHiddenFile bool) ([]FileItem, error) {
	rd, err := ioutil.ReadDir(path)
	var rt []FileItem
	if err != nil {
		return rt, err
	}
	for _, fi := range rd {
		name := fi.Name()
		isHidden := strings.HasPrefix(name, ".")
		//skip hidden file
		if !showHiddenFile && isHidden {
			continue
		}
		mode := fi.Mode()
		item := FileItem{
			Name:       name,
			IsHidden:   isHidden,
			Size:       fi.Size(),
			Permission: strings.TrimPrefix(fmt.Sprintf("%s", mode&os.ModePerm), "-"),
		}
		modeType := mode & os.ModeType
		switch modeType {
		case 0:
			item.Type = "file"
		case os.ModeDir:
			item.Type = "dir"
		case os.ModeSymlink:
			item.Type = "lnk"
		case os.ModeDevice:
			item.Type = "dev"
		case os.ModeCharDevice:
			item.Type = "char"
		case os.ModeNamedPipe:
			item.Type = "pip"
		case os.ModeSocket:
			item.Type = "socket"
		case os.ModeTemporary:
			item.Type = "temp"
		case os.ModeIrregular:
			item.Type = "na"
		}
		sysInfo := fi.Sys().(*syscall.Stat_t)
		if u, err := user.LookupId(strconv.Itoa(int(sysInfo.Uid))); nil == err {
			item.Owner = u.Username
		}
		if u, err := user.LookupGroupId(strconv.Itoa(int(sysInfo.Gid))); nil == err {
			item.Group = u.Name
		}
		item.CreateTime = time.Unix(sysInfo.Ctim.Unix()).Format(DataTimeFormatLayout)
		item.AccessTime = time.Unix(sysInfo.Atim.Unix()).Format(DataTimeFormatLayout)
		item.ModifyTime = time.Unix(sysInfo.Mtim.Unix()).Format(DataTimeFormatLayout)
		rt = append(rt, item)
	}
	return rt, err
}
