package fs

const (
	Directory = iota
	File
	SoftLink
	HardLink
)

const (
	DataTimeFormatLayout = "2006-01-02 15:04:05"
)

type FileItem struct {
	Name       string `json:"name"`
	Type       string `json:"type"`
	Owner      string `json:"owner"`
	IsHidden   bool   `json:"hidden"`
	Permission string `json:"permission"`
	Group      string `json:"group"`
	Size       int64  `json:"size"`
	ModifyTime string `json:"mtime"`
	CreateTime string `json:"ctime"`
	AccessTime string `json:"atime"`
}

type DirInfo struct {
	Name       string `json:"name"`
	Type       int    `json:"type"`
	Owner      string `json:"owner"`
	Permission string `json:"permission"`
	Group      string `json:"group"`
	Size       int64  `json:"size"`
	ModifyTime string `json:"mtime"`
	CreateTime string `json:"ctime"`
	AccessTime string `json:"atime"`
}
