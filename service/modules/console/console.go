package console

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"ucox.cn/nas/config"
)

func Index(c *gin.Context) {

	config.Logger.Debug("console index", zap.Int("result", 10))
	c.JSON(http.StatusOK, gin.H{
		"succ": true,
	})
}
