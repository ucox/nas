package system

import (
	"github.com/gin-gonic/gin"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
	"net/http"
	"strings"
	"ucox.cn/nas/config"
	"ucox.cn/nas/devkits/fs"
)

//ListDirs return directories list of specified path
func ListDirs(c *gin.Context) {
	path := c.Query("p")
	path = strings.TrimSpace(path)
	path = strings.TrimPrefix(path, ".")
	dirs, err := fs.ListDirectories("/" + strings.ReplaceAll(path, ".", "/"))
	config.Logger.Warn("加载成功")
	if nil != err {
		c.JSON(http.StatusOK, gin.H{
			"msg":  "加载失败",
			"succ": false,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"msg":  "加载成功",
		"succ": true,
		"data": dirs,
	})
}

func CpuInfo(c *gin.Context) {
	cpu, _ := cpu.Info()
	c.JSON(http.StatusOK, cpu)
}

func DiskInfo(c *gin.Context) {
	d, _ := disk.Usage("/")
	c.JSON(http.StatusOK, d)
}

func MemoryInfo(c *gin.Context) {
	v, _ := mem.VirtualMemory()
	c.JSON(http.StatusOK, v)
}

func DiskIO(c *gin.Context) {
	v, _ := disk.IOCounters()
	c.JSON(http.StatusOK, v)
}

//func listDirAll(path string) {
//	f, err := os.Open(path)
//	if err != nil {
//		return
//	}
//	names, err := f.Readdirnames(-1)
//	f.Close()
//	if err != nil {
//		return
//	}
//	sort.Strings(names)
//
//	for _, name := range names {
//		filename := filepath.Join(path, name)
//		fileInfo, _ := os.Lstat(filename)
//
//		if fileInfo.IsDir() {
//			println("dir:(" + filename + ")")
//			continue
//		}
//		if !fileInfo.IsDir() {
//			println("file:(" + filename + ")")
//			continue
//		}
//	}
//}
//
//func listDir(path string) (files []string, err error) {
//	files = make([]string, 0, 10)
//	dir, err := ioutil.ReadDir(path)
//	if nil != err {
//		return nil, err
//	}
//	for _, item := range dir {
//		if item.IsDir() {
//			files = append(files, item.Name())
//		}
//	}
//	return files, nil
//}
//
//func listFiles(path, suffix string) (files []string, err error) {
//	files = make([]string, 0, 10)
//	dir, err := ioutil.ReadDir(path)
//	if nil != err {
//		return nil, err
//	}
//	pathSep := string(os.PathListSeparator)
//	suffix = strings.ToUpper(suffix)
//
//	for _, item := range dir {
//		if item.IsDir() {
//			continue
//		}
//		if strings.HasSuffix(strings.ToUpper(item.Name()), suffix) {
//			files = append(files, path+pathSep+suffix)
//		}
//	}
//	return files, nil
//}
