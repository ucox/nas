package docker

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
)

// GetContainers get all container information
func GetContainers(c *gin.Context) {
	address := "/containers/json"
	address = address + "?all=" + strconv.Itoa(1)
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

//GetContainer. Wrap docker remote API to get data of contaienr
func GetContainer(c *gin.Context) {
	id := c.Param("id")
	address := "/containers/" + id + "/json"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// GetContainerTop. Wrap docker remote API to get container's status
func GetContainerTop(c *gin.Context) {
	id := c.Param("id")
	address := "/containers/" + id + "/top"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// StartContainer. Wrap docker remote API to start contaienr
func StartContainer(c *gin.Context) {
	id := c.Param("id")
	address := "/containers/" + id + "/start"
	result := RequestUnixSocket(address, "POST")
	c.JSON(http.StatusOK, result)
}

// StopContainer. Wrap docker remote API to stop contaienr
func StopContainer(c *gin.Context) {
	id := c.Param("id")
	address := "/containers/" + id + "/stop"
	result := RequestUnixSocket(address, "POST")
	c.JSON(http.StatusOK, result)
}

// DeleteContainer. Wrap docker remote API to delete contaienrs
func DeleteContainer(c *gin.Context) {
	id := c.Param("id")
	address := "/containers/" + id + "/start"
	result := RequestUnixSocket(address, "DELETE")
	c.JSON(http.StatusOK, result)
}

// GetContainerStats. Wrap docker remote API to get container stats
func GetContainerStats(c *gin.Context) {
	id := c.Param("id")
	address := "/containers/" + id + "/stats?stream=False"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// GetImages. Wrap docker remote API to get images
func GetImages(c *gin.Context) {
	address := "/images/json"
	result := RequestUnixSocket(address, "GET")
	list := make([]map[string]interface{}, 1)
	_ = json.Unmarshal([]byte(result), &list)
	c.JSON(http.StatusOK, list)
}

// GetImage. Wrap docker remote API to get data of image
func GetImage(c *gin.Context) {
	id := c.Param("id")
	address := "/images/" + id + "/json"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// DeleteImage. Wrap docker remote API to delete image
func DeleteImage(c *gin.Context) {
	id := c.Param("id")
	address := "/images/" + id
	result := RequestUnixSocket(address, "DELETE")
	c.JSON(http.StatusOK, result)
}

// GetVersion. Wrap docker remote API to get version info
func GetVersion(c *gin.Context) {
	address := "/version"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// GetDockerInfo. Wrap docker remote API to get docker info
func GetDockerInfo(c *gin.Context) {
	address := "/info"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// SearchImages. Wrap docker remote API to get search images
func SearchImages(c *gin.Context) {
	term := c.Query("term")
	address := "/images/search" + "?term=" + term
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// GetUserImage. Wrap docker remote API to get data of user image
func GetUserImage(c *gin.Context) {
	user := c.Query("user")
	repo := c.Query("repo")
	address := "/images/" + user + "/" + repo + "/json"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

// GetDaemonPing. Warp docker remote API to ping docker daemon
func GetDaemonPing(c *gin.Context) {
	address := "/ping"
	result := RequestUnixSocket(address, "GET")
	c.JSON(http.StatusOK, result)
}

func RequestUnixSocket(address, method string) string {
	DockerUnixSocket := "unix:///var/run/docker.sock"
	// Example: unix:///var/run/docker.sock:/images/json?since=1374067924
	unixSocketUrl := DockerUnixSocket + ":" + address
	u, err := url.Parse(unixSocketUrl)
	if err != nil || u.Scheme != "unix" {
		fmt.Println("Error to parse unix socket url " + unixSocketUrl)
		return ""
	}

	hostPath := strings.Split(u.Path, ":")
	u.Host = hostPath[0]
	u.Path = hostPath[1]

	conn, err := net.Dial("unix", u.Host)
	if err != nil {
		fmt.Println("Error to connect to", u.Host, err)
		return ""
	}

	reader := strings.NewReader("")
	query := ""
	if len(u.RawQuery) > 0 {
		query = "?" + u.RawQuery
	}

	request, err := http.NewRequest(method, u.Path+query, reader)
	if err != nil {
		fmt.Println("Error to create http request", err)
		return ""
	}

	client := httputil.NewClientConn(conn, nil)
	response, err := client.Do(request)
	if err != nil {
		fmt.Println("Error to achieve http request over unix socket", err)
		return ""
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error, get invalid body in answer")
		return ""
	}

	defer response.Body.Close()
	return string(body)
}
