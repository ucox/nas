package carte

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func SaveCategory(c *gin.Context) {
	//dto := new(DTOCategories)
	//dto.ParentId = 9999
	//dto.Title = "标题"
	//dto.Count = 100
	//dto.Url = "http://ucox.cn"

	dto := &DTOCategories{
		Category: Category{
			ParentId: 8888,
			Title:    "8888Title",
			Url:      "http://baidu.com",
		},
		Count: 1000,
	}

	dto.Save()
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": dto.Category.Id,
	})
}

func Categories(c *gin.Context) {
	categories := &CategoryList{}
	if nil != categories.GetCategories() {
		c.JSON(http.StatusOK, gin.H{
			"code": 5001,
			"msg":  "获取菜单分类失败",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": categories,
	})
}
