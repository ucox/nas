package carte

import (
	"go.uber.org/zap"
	. "ucox.cn/nas/config"
)

type DTOCategories struct {
	Category
	Count int `xorm:"children"`
}

func (c *DTOCategories) Save() {
	res, err := ORM.
		Exec(`insert into t_carte_categories(p_id,title,url) values(?,?,?)`, c.ParentId, c.Title, c.Url)

	if nil != err {
		Logger.Error("save category error", zap.Error(err))
		return
	}
	id, err := res.LastInsertId()
	Logger.Debug("create new category", zap.Int64("id", id))
}
