package carte

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"ucox.cn/nas/config"
)

type Category struct {
	Id       int    `json:"id"`
	ParentId int    `json:"pid",,xorm:"p_id"`
	Title    string `json:"title"`
	Url      string `json:"url"`
}

type CategoryList []Category

func (c *CategoryList) add(parentId int, parentTitle, title, url string) {
	*c = append(*c, Category{
		ParentId: parentId,
		Title:    title,
		Url:      url,
	})
}

func (c *CategoryList) GetCategories() (err error) {
	spiderConfig := config.App.SpiderConfig

	res, err := http.Get(spiderConfig.FetchDomain + "/recipe-type.html")
	if err != nil {
		log.Print(err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Printf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Print(err)
	}

	doc.Find(".category_sub").Each(func(index int, item *goquery.Selection) {
		parentTitle := item.Find("h3").Text()
		item.Find("li").Each(func(childIdx int, childItem *goquery.Selection) {
			eleA := childItem.Find("a")
			url, _ := eleA.Attr("href")
			fmt.Printf("insert into t_carte")
			c.add(index, parentTitle, eleA.Text(), url)
		})
	})
	return
}
