package profile

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

//TokenIssue issue a token for terminal.
func TokenIssue(c *gin.Context) {
	userName := c.Query("u")
	password := c.Query("p")
	halfHours, _ := time.ParseDuration("30m")
	hours, _ := time.ParseDuration("60m")
	now := time.Now()
	fmt.Println("userName=", userName, ",", "password=", password, "time=", now)
	fmt.Println(time.Unix(1547804377, 0).Format("2006-01-02 15:04:05"))
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"data": gin.H{
			"access_token": map[string]interface{}{
				"aud":   "http://api.example.com",
				"iss":   "https://myapp.example.com",
				"sub":   "1234567890qwertyuio",
				"jti":   "mnb23vcsrt756yuiomnbvcx98ertyuiop",
				"roles": []string{"user", "admin"},
				"profile": map[string]interface{}{
					"uid":  123,
					"dept": 456,
				},
				"exp": now.Add(halfHours).Unix(),
			},
			"refresh_token": map[string]interface{}{
				"aud": "http://api.example.com",
				"iss": "https://myapp.example.com",
				"sub": "1234567890qwertyuio",
				"jti": "mnb23vcsrt756yuiomn12876bvcx98ertyuiop",
				"exp": now.Add(hours).Unix(),
			},
		},
	})
}

func TokenSymmetric(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"keys": []interface{}{
			map[string]interface{}{
				"kid": "sim1",
				"kty": "oct",
				"alg": "A128KW",
				"k":   "GawgguFyGrWKav7AX4VKUg",
			},
			map[string]interface{}{
				"kid": "sim2",
				"kty": "oct",
				"alg": "HS256",
				"k":   "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow",
			},
		},
	})
}
