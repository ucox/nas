package config

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
	"time"
)

type spiderConfig struct {
	FetchDomain string `map:"fetch_domain"`
}

type serverConfig struct {
	Name    string `map:"name"`
	Mode    string `map:"mode"`
	Port    int    `map:"port"`
	Pid     string `map:"pid"`
	WebRoot string `map:"web_root"`
}

type logConfig struct {
	Level     int8   `map:"level"`
	FileName  string `map:"file_name"`
	MaxSize   int    `map:"max_size"`
	MaxBackup int    `map:"max_backup"`
	MaxAge    int    `map:"max_age"`
	Compress  bool   `map:"compress"`
}

type databaseConfig struct {
	Dialect           string `map:"dialect"`
	Host              string `map:"host"`
	Database          string `map:"database"`
	Port              int    `map:"port"`
	User              string `map:"user"`
	Password          string `map:"password"`
	MaxConnection     int    `map:"max_connection"`
	MaxIdleConnection int    `map:"max_idle_connection"`
	ShowSQL           bool   `map:"show_sql"`
}

type appConfig struct {
	ServerConfig        serverConfig        `map:"server"`
	LogConfig           logConfig           `map:"log"`
	SpiderConfig        spiderConfig        `map:"spider"`
	DatabaseConfig      databaseConfig      `map:"database"`
	SensitiveWordConfig sensitiveWordConfig `map:"sensitive_word"`
}

type sensitiveWordConfig struct {
	DictFile string `map:"word_dic"`
}

const (
	// DebugMode indicates gin mode is debug.
	DebugMode = "debug"
	// ReleaseMode indicates gin mode is release.
	ReleaseMode = "release"
	// TestMode indicates gin mode is test.
	TestMode = "test"
)

var (
	App    *appConfig
	ORM    *xorm.Engine
	Logger *zap.Logger
)

func init() {
	//flag.StringVar(&config, "d", "./", "set logic config file path")
	initConfig()
	initLogger()
	//initDatabase()
}

func initDatabase() {
	var err error
	serverConfig := App.ServerConfig
	dbConfig := App.DatabaseConfig
	connUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.Database)
	Logger.Debug("db connection url", zap.String("url", connUrl))
	ORM, err = xorm.NewEngine(dbConfig.Dialect, connUrl)

	if nil != err {
		panic(fmt.Errorf("unable to establishing database connection: %s\n", err))
	}

	ORM.ShowSQL(dbConfig.ShowSQL)
	ORM.ShowExecTime(ReleaseMode != serverConfig.Mode)
	ORM.Logger().ShowSQL(dbConfig.ShowSQL)
	ORM.Logger().SetLevel(core.LOG_DEBUG)

	ORM.SetMaxOpenConns(dbConfig.MaxConnection)
	ORM.SetMaxIdleConns(dbConfig.MaxIdleConnection)
}

func initConfig() {
	App = &appConfig{
		ServerConfig: serverConfig{
			Mode:    ReleaseMode,
			Pid:     "/tmp/nas.pid",
			WebRoot: "/",
		},
		SpiderConfig: spiderConfig{},
		DatabaseConfig: databaseConfig{
			ShowSQL: false,
		}, /*,
		SensitiveWordConfig: SensitiveWordConfig{
			DictFile: "/etc/nas/sensitive_work.dic",
		},*/
	}

	viper.SetConfigName("nas")
	viper.SetConfigType("toml")
	viper.AddConfigPath("./")

	if err := viper.ReadInConfig(); nil != err {
		panic(fmt.Errorf("unable to read config: %s \n", err))
	}

	if err := viper.Unmarshal(&App, func(config *mapstructure.DecoderConfig) {
		config.TagName = "map"
	}); nil != err {
		panic(fmt.Errorf("unable to decode into struct: %s\n", err))
	}
}

func initLogger() {
	logConfig := App.LogConfig

	hook := lumberjack.Logger{
		Filename:   logConfig.FileName,
		MaxSize:    logConfig.MaxSize,
		MaxBackups: logConfig.MaxBackup,
		MaxAge:     logConfig.MaxAge,
		Compress:   logConfig.Compress,
	}

	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "line",
		MessageKey:     "msg",
		StacktraceKey:  "trace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
		EncodeName:     zapcore.FullNameEncoder,
	}

	//setup log level
	atomicLevel := zap.NewAtomicLevel()
	atomicLevel.SetLevel(zapcore.Level(logConfig.Level))

	//setup core config
	if gin.DebugMode == App.ServerConfig.Mode {
		encoderConfig.EncodeTime = func(time time.Time, encoder zapcore.PrimitiveArrayEncoder) {
			encoder.AppendString(time.Format("2006-01-02 15:04:05.000"))
		}

		zapCore := zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderConfig),
			zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(&hook)),
			atomicLevel)
		//enable develop mode and stack trace
		caller := zap.AddCaller()

		//enable line number display
		development := zap.Development()

		//setup init field
		//field := zap.Fields(zap.String("service", "nas"))
		//construct log
		Logger = zap.New(zapCore, caller, development)
	} else {
		zapCore := zapcore.NewCore(
			zapcore.NewConsoleEncoder(encoderConfig),
			zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(&hook)),
			atomicLevel)
		//Logger, _ = zap.NewProduction()
		field := zap.Fields(zap.String("node", "n1"))
		Logger = zap.New(zapCore, field)
	}
	Logger.Info("log init success!")
	defer Logger.Sync()
}
