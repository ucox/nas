package main

import (
	"github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strconv"
	"time"
	. "ucox.cn/nas/config"
	"ucox.cn/nas/route"
)

type User struct {
	ID   string
	Name string
}

func initServer() *gin.Engine {
	engine := gin.New()
	if gin.DebugMode == App.ServerConfig.Mode {
		gin.SetMode(gin.ReleaseMode)
		//engine.Use(ginzap.Ginzap(Logger, "2006-01-02 15:04:05.000", false))
		engine.Use(ginzap.RecoveryWithZap(Logger, false))
	} else {
		gin.SetMode(gin.ReleaseMode)
		gin.DisableConsoleColor()
		engine.Use(ginzap.Ginzap(Logger, time.RFC3339, true))
		engine.Use(ginzap.RecoveryWithZap(Logger, false))
	}

	route.RegisterSystemApi(engine)
	route.RegisterConsoleApi(engine)
	route.RegisterDockerApi(engine)
	route.RegisterCarteApi(engine)
	return engine
}

func main() {
	r := initServer()
	Logger.Info("server start success", zap.Int("port", App.ServerConfig.Port))
	_ = r.Run(":" + strconv.Itoa(App.ServerConfig.Port))
}
