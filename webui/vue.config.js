const path = require('path');
module.exports = {
    publicPath: '/',
    outputDir: 'dist',
    assetsDir: 'assets',
    productionSourceMap: false,
    lintOnSave: false,
    chainWebpack: (config) => {
        config.resolve.alias
            .set('src', path.join(__dirname, 'src'))
    },
    css: {
        loaderOptions: {
            css: {
                test: /\.less$/,
                loader: 'less-loader'
            }
        }
    },
    devServer: {
        host: 'localhost',
        port: 4000,
        https: false,
        proxy: {
            '^/api': {
                target: 'http://localhost:9999/',
                changeOrigin: false,
            }
        }
    }
}
