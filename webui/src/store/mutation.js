import {CHANGE_ACTIVE_APP_TITLE, TOGGLE_EXPLORER_DISPLAY} from "src/store/mutation-types";

export default {
    [CHANGE_ACTIVE_APP_TITLE](state, title) {
        state.title = title
    },
    [TOGGLE_EXPLORER_DISPLAY](state, display) {
        state.explorer.display = display
    }
}
