import Vue from 'vue'
import Vuex from 'vuex'
import getters from 'src/store/getters'
import mutations from 'src/store/mutation'

Vue.use(Vuex)

const state = {
    debug: true,
    title: 'Finder',
    loginUser: null,
    explorer: {
        display: false,
        x: 10,
        y: 20
    },
    icon: {
        size: 20
    }

}

export default new Vuex.Store({
    state,
    getters,
    mutations
})

