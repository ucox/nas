// import store from 'src/store'
import axios from 'axios/'
import baseURL from 'src/config'

// const addErrorLog = errorInfo => {
//     const {statusText, status, request: {responseURL}} = errorInfo
//     let info = {
//         type: 'ajax',
//         code: status,
//         mes: statusText,
//         url: responseURL
//     }
//     if (!responseURL.includes('save_error_logger')) store.dispatch('addErrorLog', info)
// }

class HttpRequest {
    constructor(baseUrl = baseURL) {
        this.baseUrl = baseUrl
        this.queue = {}
    }

    getInsideConfig() {
        return {
            baseURL: this.baseUrl,
            headers: {
                //
            }
        }
    }

    destroy(url) {
        delete this.queue[url]
        if (!Object.keys(this.queue).length) {
            // Spin.hide()
        }
    }

    refreshToken(token) {

    }

    saveToken(token) {

    }

    interceptors(instance, url) {
        // 请求拦截
        instance.interceptors.request.use(config => {
            // 添加全局的loading...
            if (!Object.keys(this.queue).length) {
                // Spin.show() // 不建议开启，因为界面不友好
            }
            this.queue[url] = true

            //请求token处理
            //判断localstorage是否存在expire
            // debugger
            // const expire = localStorage.getItem("expire")
            // const expireRemain = 20, now = new Date().getTime()
            // if (!expire) {
            //     //判断是否存在accessToken
            //     const accessToken = localStorage.getItem("access_token")
            //
            //     if (!accessToken) {
            //         //TODO: 跳转至登录页面
            //     }
            // }
            // //检测accessToken是否过期，如果即将过期则重新刷新
            // if (Math.abs(now - expire) < expireRemain) {
            //     const refreshToken = localStorage.getItem("refresh_token")
            //     const newToken = this.refreshToken(refreshToken)
            //     //重新存储token
            //     this.saveToken(newToken)
            // }
            return config
        }, error => {
            return Promise.reject(error)
        })
        // 响应拦截
        instance.interceptors.response.use(res => {
            this.destroy(url)
            const {data, status} = res
            return {data, status}
        }, error => {
            this.destroy(url)
            let errorInfo = error.response
            if (!errorInfo) {
                const {request: {statusText, status}, config} = JSON.parse(JSON.stringify(error))
                errorInfo = {
                    statusText,
                    status,
                    request: {responseURL: config.url}
                }
            }
            // addErrorLog(errorInfo)
            return Promise.reject(error)
        })
    }

    request(options) {
        const instance = axios.create()
        options = Object.assign(this.getInsideConfig(), options)
        this.interceptors(instance, options.url)
        return instance(options)
    }
}

export default HttpRequest
