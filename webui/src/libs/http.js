import HttpRequest from 'src/libs/axios'
import config from 'src/config'

const baseUrl = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : config.baseUrl.pro

const Http = new HttpRequest(baseUrl)
export default Http