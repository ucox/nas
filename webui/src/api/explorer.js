import Http from 'src/libs/http'

export const listRootDir = () => {
    return Http.request({
        url: 'api/dir',
        method: 'get',
    })
}