import Http from 'src/libs/http'

export const doLogin = (userName, password) => {
    return Http.request({
        url: `api/v1/token?u=${userName}&p=${password}`,
        method: 'get',
    })
}