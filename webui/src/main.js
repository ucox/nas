import './theme/index.less'
import Vue from 'vue'
import App from 'src/App.vue'
import ContextMenu from 'src/components/ContextMenu.vue'
import store from 'src/store'

Vue.config.productionTip = true
Vue.use(ContextMenu)

new Vue({
    render: h => h(App),
    store,
}).$mount('#app')
